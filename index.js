import DebugString from '@wingnut/DebugString'
import Exception   from '@wingnut/Exception'
import IsString    from '@wingnut/IsString'



const regExp = /^[A-Z]([a-z]+[A-Z]?)*[0-9]*$/


export default x => (
	IsString(x)
	? regExp.test(x)
	: Exception(Error(`IsPascalCase : the first and only argument must be a string, received ${DebugString(x)}`))
)
